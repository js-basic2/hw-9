
"use strict";

// Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// Створити новий тег на сторінці можно так:
// let div = document.createElement('div');
// Але поки що тегу немає на сторінці, щоб його побачити треба додати:
// document.body.append(div)
// Тепер div з'явиться в body.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Перший параметр функції elem.insertAdjacentHTML - where, вказує, куди треба вставляти новий елемент. Приймає 4 значення:
// "beforebegin" - вставити елемент перед elem 
// "afterbegin" - вставити елемент в початок elem
// "beforeend" - вставити елемент в кінець elem
// "afterend" - вставити елемент після elem

// 3. Як можна видалити елемент зі сторінки?
// За допомогою метода elem.remove();
// Замість elem вставити потрібний елемент, який треба видалити.

let arr = ["1", "2", "3", "sea", "user", 23];
//let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


function getListContent(arr, parent='body'){
    let par = document.querySelector(parent);
    let ul = document.createElement('ul');
    par.prepend(ul);
    let listItem = arr.map((item) => `<li>${item}</li>`).join('');
    ul.insertAdjacentHTML('afterbegin', listItem);
}


getListContent(arr);
getListContent (arr, 'div');
getListContent (arr, 'p');





    

